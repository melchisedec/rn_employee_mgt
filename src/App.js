import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import reducers from './reducers';
// import LoginForm from './components/LoginForm';
import Router from './Router';


class App extends Component {
    
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyABZDaYnSHDd5KYz93e4kT98MHAwMgXRAc',
            authDomain: 'employeemgt-7c3a1.firebaseapp.com',
            databaseURL: 'https://employeemgt-7c3a1.firebaseio.com',
            projectId: 'employeemgt-7c3a1',
            storageBucket: 'employeemgt-7c3a1.appspot.com',
            messagingSenderId: '1075804527645'
          };
        firebase.initializeApp(config);
    }
    render() {
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
